<?php
include "cookie_login_check.php";
if(cookie_login_check()){


//Checks the cookies to see
$cookie_user = 'user_cookie';
$cookie_password = 'password_cookie';

$conn = new mysqli("localhost", "root", "CHANGEPASSWORD", "cheeseshopDB");
// Check connection
if (mysqli_connect_errno())
{
//echo "Failed to connect to MySQL: " . mysqli_connect_error();
}

// get the q parameter from URL
$searchword = $_REQUEST["q"];
$param = "%$searchword%";
	


// Checks what privilege logged in account has and prints all orders if admin otherwise only personal orders.
if($result = mysqli_query($conn, "Select Privilege FROM Users WHERE UserID = $_COOKIE[$cookie_user]")){
    $row = mysqli_fetch_array($result);

    if($row["Privilege"] == 2){ //User is admin
        //Depending on searchword
        if($searchword == "" || $searchword == "All"){
            //echo "Searching all <br>";
            $result = mysqli_query($conn,"SELECT * FROM Orders");   
        
        } else{
            //echo "Searching by $searchword <br>";
        	$stmt = $conn->prepare("SELECT * FROM Orders WHERE Address LIKE ? OR OrderID LIKE ?");
        	$stmt->bind_param("si", $param, $searchword);
        	$stmt->execute();
        	$result = $stmt->get_result();
        }
    } else{ //User is not an admin
        //Depending on searchword
        if($searchword == "" || $searchword == "All"){
            //echo "Searching all <br>";
            $result = mysqli_query($conn,"SELECT * FROM Orders WHERE UserID = $_COOKIE[$cookie_user]");   
        
        } else{
            //echo "Searching by $searchword <br>";
        	$stmt = $conn->prepare("SELECT * FROM Orders WHERE (Address LIKE ? OR OrderID LIKE ?) AND UserID = ?");
        	$stmt->bind_param("sii", $param, $searchword,$_COOKIE[$cookie_user]);
        	$stmt->execute();
        	$result = $stmt->get_result();
        }
    }
    $orderrow = "";
    while($row = mysqli_fetch_array($result)){
                $s = "$row[OrderID]";
                $orderrow = $orderrow . "<tr>
                <td> $row[OrderID] </td>
                <td>". date("Y-m-d", $row['OrderDate']) ."</td>
                <td>". date("Y-m-d", $row['ArriveDate']) ."</td>
                <td> $row[Address] </td>
                <td> $row[Priority]</td>
                <td> $row[TotalPrice] </td>
                <td> $row[Status] 
                        <button name=$s onclick=nextstatus(this.name)>+</button>
                </td>
                <td>
                        <button name=$s onclick=removeorder(this.name)>Remove</button>
                        <button name=$s onclick=showitems(this.name)>More</button>
                </td>
                    
                </tr>";
            }
   echo "$orderrow";
    }
    
else{
echo "You are not logged in.";
}
mysqli_close($conn);
}
?>