<?php
include "cookie_login_check.php";
if(cookie_login_check()){

//Checks the cookies
$cookie_user = 'user_cookie';
$cookie_password = 'password_cookie';

if(!isset($_COOKIE[$cookie_password]) || !isset($_COOKIE[$cookie_user])) {
        echo "Cookie with names  $cookie_user $cookie_password  does not exist";
} else {
        $conn = new mysqli("localhost", "root", "CHANGEPASSWORD", "cheeseshopDB");
        if($conn->connect_error) {
                die("Could not connect".$conn->connect_error);
        }
        //Tries to cookie login
        if($stmt = $conn->prepare("SELECT Privilege FROM Users WHERE UserID = ? AND Password = ?")) {
                $stmt->bind_param("is", $_COOKIE[$cookie_user], $_COOKIE[$cookie_password]);
                $stmt->execute();
                $result = $stmt->get_result();
                if($result->num_rows === 0) { //Checks if anything returned
                        echo "Cookie login failed";
                } else {
                        $row = $result->fetch_assoc();
                        //Checks if logged in user is a seller
                        if(!($row["Privilege"] == 1)){
                                echo "You are not a seller account.";
                        }
                        else{
                                //echo "Record is being created";
                                //getting target location for the file upload
                                
                                $currentDir = getcwd();
                                $targetDir = "/var/www/html/images/"; 
                                $pic = $_FILES['Image']['name']; 
                                $path = $targetDir . basename($pic);
                                
                                //Posts the ware specified
                                try{
                                    $conn->begin_transaction();
                                    if($stmt = $conn->prepare("INSERT INTO Wares(SellerID, Item, Type, Description, Image, Grade, NumGrade, Price, Quantity, Discount) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")) {
                                        $stmt->bind_param("issssiiiii", $_COOKIE[$cookie_user], $_POST["Item"], $_POST["Type"], $_POST["Description"], $pic, $a = 0, $a1 = 0, $_POST["Price"], $_POST["Quantity"], $_POST["Discount"]);
                                        $stmt->execute();
                                        $conn->commit();
                                        //echo "Record created <br>";
                                        
                                        if(move_uploaded_file($_FILES['Image']['tmp_name'],$path)){
                                            //echo "The file ". basename( $_FILES['Image']['name']). " has been uploaded, and your information has been added to the directory"; 
                                            echo "Item has been added to the store.<br><a href='index.php'>Click to go back to home page</a>";
                                        } else { 
                                            echo "Sorry, there was a problem uploading your file."; 
                                        }
                                        include '/var/php/create_listing_page.php';
                                        $conn->close();
                                    }
                                } catch (Exception $e) {
                                    $conn->rollback();
                                }
                        }
                }
        }
}
}
?>