<html>
<body>

<?php
$servername="localhost";
$username="root";
$password="CHANGEPASSWORD";
$dbname="cheeseshopDB";

$conn = new mysqli($servername, $username, $password, $dbname);

if($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
}

//FName, Email, Password required. Checks here if missing
if(empty($_POST["FName"]) or empty($_POST["Email"]) or empty($_POST["Password"])) {
        echo "Form not completely filled in. Please refill in the form";
        header('Location: http://130.240.200.65/fail_acc.html');
} else {
        if($checkStmt = $conn->prepare("SELECT Email FROM Users WHERE Email=?")) {
                $checkStmt->bind_param("s", $_POST["Email"]);
                $checkStmt->execute();
                $res = $checkStmt->get_result();
                if($res->num_rows == 0) {
                        try {
                            $conn->begin_transaction();
                            if($stmt = $conn->prepare("INSERT INTO Users(FName, LName, Email, Address, Privilege, Password) VALUES(?, ?, ?, ?, ?, ?)")) {
                                $EncryptedPass = password_hash($_POST["Password"], PASSWORD_DEFAULT);
                                $stmt->bind_param("ssssis", $_POST["FName"], $_POST["LName"], $_POST["Email"], $_POST["Address"], $a = 0, $EncryptedPass);
                                $stmt->execute();
                                $conn->commit();
                                echo "Record created";

                                header('Location: http://130.240.200.65/success_acc.html');
                            } else {
                                echo "Record not created";
                            }
                        } catch (Exception $e) {
                            $conn->rollback();
                        }
                        //Automatically login user
                        if($stmt_id = $conn->prepare("SELECT UserID, Password FROM Users WHERE Email=?")) {
                            $stmt_id ->bind_param("s", $_POST["Email"]);
                            $stmt_id ->execute();
                            $res = $stmt_id->get_result();
                            $row = $res->fetch_assoc();
                            $cookie_user = 'user_cookie';
                			$cookie_password = 'password_cookie';
                			$cookie_name = 'name_cookie';
                		    $cookie_email = 'email_cookie';
                		    $cookie_privilege = 'privilege_cookie';
                			setcookie($cookie_user, $row["UserID"], time() + (86400 * 30), "/"); // 86400 = 1 day
                			setcookie($cookie_password, $row["Password"], time() + (86400 * 30), "/"); // 86400 = 1 day
                			setcookie($cookie_name, $_POST["FName"], time() + (86400 * 30), "/"); // 86400 = 1 day
                			setcookie($cookie_email, $_POST["Email"], time() + (86400 * 30), "/"); // 86400 = 1 day
                			setcookie($cookie_privilege, 'Buyer', time() + (86400 * 30), "/"); // 86400 = 1 day 
                        } else {
                            echo "Prepared statement error";
                        }

                } else {
                        echo "That email already exists";
                }
        }
}
$conn->close();
?>

</body>
</html>


