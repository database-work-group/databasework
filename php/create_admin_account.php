<?php

	$cookie_user = 'user_cookie';
	$cookie_password = 'password_cookie';

	if(!isset($_COOKIE[$cookie_password]) || !isset($_COOKIE[$cookie_user])) {
	  echo "Cookie with names $cookie_user $cookie_password does not exist...";
	} else {
		
	$conn = new mysqli("localhost", "root", "CHANGEPASSWORD", "cheeseshopDB");
	//$EncryptedPass = password_hash($_POST["Password"], PASSWORD_DEFAULT);
		//Tries to cookie login
		if($stmt = $conn->prepare("SELECT Password FROM Users WHERE UserID = ?")) {
			$stmt->bind_param("i", $_COOKIE[$cookie_user]);
			$stmt->execute();
			$result = $stmt->get_result();
			
			if($result->num_rows === 0) { //Checks if anything returned
				echo "Cookie login failed";
			} else {
				$row = $result->fetch_assoc();
				if(!password_verify($_POST["Password"], $row["Password"])){
					echo "That is not the password for your account.";
				} else {
					if($stmt = $conn->prepare("SELECT * FROM AdminCode WHERE aCode = ?")) {
						$stmt->bind_param("s", $_POST["aCode"]);
						$stmt->execute();
						$result = $stmt->get_result();
						$row = $result->fetch_assoc();
						if($result->num_rows === 0) { //Checks if anything returned
							echo "Admin code is wrong";
						} else {
							try {
								$conn->begin_transaction();
								if($sql = $conn->prepare("UPDATE Users SET Privilege = 2 WHERE UserID = ?")) {
									$sql->bind_param("s", $_COOKIE[$cookie_user]);
									$sql->execute();
									$conn->commit();
									echo "Record updated successfully. <br>";
									echo "You are now an admin.<br>";
									echo "<a href='/index.php'>Click to go back to the home page</a>";
									setcookie("privilege_cookie", "Admin", time()+ 24*60*60); 
								} else {
									echo "Error updating record";
								}
							} catch (Exception $e) {
								$conn->rollback();
							}
						}
					}
				}	
			}
				
			
		}
	}
$conn->close();
?>
