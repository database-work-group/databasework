<?php
include "cookie_login_check.php";
include "is_item_available.php";
if(cookie_login_check()){

$cookie_user = "user_cookie";
$cookie_password = "password_cookie";

if(!isset($_COOKIE[$cookie_user]) || !isset($_COOKIE[$cookie_password])) {
    echo "You are not logged in. Please login";
} else {
    $conn = new mysqli("localhost", "root", "CHANGEPASSWORD", "cheeseshopDB");
    if($conn->connect_error) {
        die("Could not connect".$conn->connect_error);
    }
    if(empty($_POST["Address"]) or empty($_POST["CredCard"]) or empty($_POST["PriShip"])) {
        echo "Please fill in the whole form";
    } else {
        //Check if quantity of items in the cart do not exceed those in the wares
        if(!is_item_available()) {
            die("One or more of your items in your cart have a greater quantity than that available in the wares.");
        }
        $TotalPrice = 0;
        $OrderDate = time();
        if($_POST["PriShip"] == 2) { //No Priority Shipping
                $ArriveDate = $OrderDate + (86400 * 7);
        } else {
            $ArriveDate = $OrderDate + (86400);
        }
        //Calculate Total Price here
        if($stmt2 = $conn->prepare("SELECT Wares.Price, Cart.Quantity, Cart.CartID, Wares.Discount FROM Wares INNER JOIN Cart ON Cart.ObjectID = Wares.ObjectID WHERE CartID=?")) {
            $stmt2->bind_param("i", $_COOKIE["user_cookie"]);
            $stmt2->execute();
            if($res = $stmt2->get_result()) {
                while($row = $res->fetch_assoc()) {
                    $PriceItem = $row["Price"];
                    $QuantItem = $row["Quantity"];
		            $Discount = $row["Discount"]/100;
                    $TotalPrice += ($PriceItem-($PriceItem*$Discount))  * $QuantItem;
                }
            } else {
                die("Error with retrieving data:".$conn->error);
            }
            $stmt2->close();

        } else {
            die("Prepared statement 2 error");
        }

        //Create Orders row
        try {
            $conn->begin_transaction();
            if($stmt = $conn->prepare("INSERT INTO Orders(UserID, Address, OrderDate, ArriveDate, Priority, TotalPrice, Status) VALUES (?,?,?,?,?,?,?)")) {
                $stmt->bind_param("isiiiii", $_COOKIE["user_cookie"], $_POST["Address"],  $OrderDate, $ArriveDate, $_POST["PriShip"], $TotalPrice, $a = 0);
                $stmt->execute();
                echo "Charged ", $TotalPrice, ". Item will arrive on ", date("Y-m-d", $ArriveDate), "<br><a href='/index.php'>Click to go to the homepage</a>";
                $stmt->close();
            } else {
                die("Prepared statement 1 error");
            }


            //Create Ordered_items rows based on Orders row
            if($stmt3 = $conn->prepare("SELECT OrderID FROM Orders WHERE UserID = ? AND OrderDate = ?")) {
                $stmt3->bind_param("ii", $_COOKIE["user_cookie"], $OrderDate);
                $stmt3->execute();
                $res = $stmt3->get_result();
                $row = $res->fetch_assoc();
                $OrderID = $row["OrderID"];
                $stmt3->close();
                if($stmt4 = $conn->prepare("SELECT Wares.ObjectID, Wares.Price, Cart.Quantity, Cart.CartID FROM Wares INNER JOIN Cart ON Cart.ObjectID = Wares.ObjectID WHERE CartID=?")) {
                    $stmt4->bind_param("i", $_COOKIE["user_cookie"]);
                    $stmt4->execute();
                    $res = $stmt4->get_result();
                    while($row = $res->fetch_assoc()) {
                        if($stmt5 = $conn->prepare("INSERT INTO Ordered_Items(OrderID, ObjectID, Quantity, Price) VALUES(?, ?, ?, ?)")) {
                            $ObjectID = $row["ObjectID"];
                            $CartQuantity =  $row["Quantity"];
                            $UserID = $row["CartID"];
                            $stmt5->bind_param("iiii", $OrderID, $ObjectID, $CartQuantity, $row["Price"]);
                            $stmt5->execute();
                            $stmt5->close();
                            //First reduce quantity from wares
                            if($stmt6 = $conn->prepare("UPDATE Wares SET Quantity = Quantity - $CartQuantity WHERE ObjectID = ?")) {
                                $stmt6->bind_param("i", $ObjectID);
                                $stmt6->execute();
                                $stmt6->close();
                            } else {
                                die("Prepared statement 6 error".$conn->error);
                            }
                            //Then remove from cart
                            if($stmt7 = $conn->prepare("DELETE FROM Cart WHERE CartID = ?")) {
                                $stmt7->bind_param("i", $UserID);
                                $stmt7->execute();
                                $stmt7->close();
                            } else {
                                die("Prepared statement 7 error".$conn->error);
                            }
                        } else {
                            die("Prepared statement 5 error".$conn->error);
                        }
                    }
                    $conn->commit();
                    $stmt4->close();
                } else {
                    die("Prepared statement 4 error");
                }
            } else {
                die("Prepared statement 3 error");
            }
        } catch (Exception $e) {
            $conn->rollback();
        }
    }
    $conn->close();
}
}

?>
