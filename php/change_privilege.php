<?php
include "cookie_login_check.php";
if(cookie_login_check()){
//Checks the cookies to see
$cookie_user = 'user_cookie';
$cookie_password = 'password_cookie';

if(!isset($_COOKIE[$cookie_password]) || !isset($_COOKIE[$cookie_user])) {
	echo 'Cookie with names: ', $cookie_user, $cookie_password, ' does not exist...';
} else {
	$conn = new mysqli("localhost", "root", "CHANGEPASSWORD", "cheeseshopDB");
	if($conn->connect_error) {
		die("Could not connect".$conn->connect_error);
	}

	//Tries to cookie login
	if($stmt = $conn->prepare("SELECT Privilege FROM Users WHERE UserID = ? AND Password = ?")) {
		$stmt->bind_param("ss", $_COOKIE[$cookie_user], $_COOKIE[$cookie_password]);
		$stmt->execute();
		$result = $stmt->get_result();
		if($result->num_rows === 0) { //Checks if anything returned
			echo "Cookie login failed";
		} else {
			$row = $result->fetch_assoc();
			//Checks if logged in user is an admin
			if(!($row["Privilege"] == 2)){
				echo "You are not an admin account";
			} else {
				//Changes the typed users privilege
				try {
					$conn->begin_transaction();
					$sql = $conn->prepare("UPDATE Users SET Privilege = ?  WHERE Email = ?");
					$sql->bind_param("is", $a = 1, $_POST["Email"]);
					$sql->execute();
					$conn->commit();
					echo "User privilege has been updated.<br>";
					echo "<a href='/index.php'>Click to go back to the home page</a>";
				} catch (Exception $e) {
					$conn->rollback();
				}
			}
		}
	}
	$conn->close();
}
//$conn->close();
}
?>
