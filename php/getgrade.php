<?php
$conn = new mysqli("localhost", "root", "CHANGEPASSWORD", "cheeseshopDB");
// Check connection
if (mysqli_connect_errno()) {
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
}

$ObjectID = $_REQUEST["q"];
if($stmt = $conn->prepare("SELECT Grade FROM Wares WHERE ObjectID = ?")) {
    $stmt->bind_param("i", $ObjectID);
    $stmt->execute();
    $res = $stmt->get_result();
    $row = $res->fetch_assoc();
    echo $row["Grade"];
} else {
    echo "Failed to prepare statement";
}
?>