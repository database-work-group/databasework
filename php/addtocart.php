<?php
include "cookie_login_check.php";
if(cookie_login_check()){
$cookie_user = 'user_cookie';
$cookie_password = 'password_cookie';

    
$conn = new mysqli("localhost", "root", "CHANGEPASSWORD", "cheeseshopDB");
// Check connection
if (mysqli_connect_errno())
{
//echo "Failed to connect to MySQL: " . mysqli_connect_error();
}

$id = $_REQUEST["id"];
$Q = $_REQUEST["q"];

if($stmt = $conn->prepare("SELECT Quantity FROM Wares Where ObjectID = ?")) {
	$stmt->bind_param("i", $id);
	$stmt->execute();
	$result = $stmt->get_result();

    $row = mysqli_fetch_array($result);
    
    if($Q > $row["Quantity"]){
        echo "You can't add that many items of this ware. Lower your quantity specification please. <br>";
    } else{
        //If there allready exists an item with that ID, Just increase the quantity
        if($stmt = $conn->prepare("SELECT ObjectID, Quantity FROM Cart WHERE ObjectID = ? AND CartID = ?")) {
    	$stmt->bind_param("ii", $id, $_COOKIE[$cookie_user]);
    	$stmt->execute();
    	$a = $stmt->get_result();
    	
        if($ro = mysqli_fetch_array($a)){
            //if the assigned quantity + the quantity allready added is more than the quantity available it wont add
            if(($Q + $ro["Quantity"]) > $row["Quantity"]){} else{
                
                //Updates an allready existing ware in the cart
                try {
					$conn->begin_transaction();
					$stmt = $conn->prepare("UPDATE Cart SET Quantity = Quantity + ? WHERE ObjectID = ? AND CartID = ?");
                    $stmt->bind_param("iii",$Q, $id, $_COOKIE[$cookie_user]);
                    $stmt->execute();
                    $conn->commit();
                    echo "Record updated successfully \n";
				} catch (Exception $e) {
					$conn->rollback();
					echo "Error updating record: " . mysqli_error($conn);
				}
  
                
            }
        } else{
            //Create a new record
                try {
					$conn->begin_transaction();
					$stmt = $conn->prepare("INSERT INTO Cart(CartID, ObjectID, Quantity) VALUES(?, ?, ?)");
                    $stmt->bind_param("iii", $_COOKIE[$cookie_user], $id,$Q);
                    $stmt->execute();
                    $conn->commit();
                    echo "<br/> $id The item has been added to your cart. Amount $Q";
				} catch (Exception $e) {
					$conn->rollback();
					echo "Record not created";
				}
            }
        }
        
    }
}
mysqli_close($conn);
}
?>