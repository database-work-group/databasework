<?php
include "cookie_login_check.php";
if(cookie_login_check()){


//Checks the cookies to see
$cookie_user = 'user_cookie';
$cookie_password = 'password_cookie';

//Sets up connection
$conn = new mysqli("localhost", "root", "CHANGEPASSWORD", "cheeseshopDB");
// Check connection
if (mysqli_connect_errno())
{
//echo "Failed to connect to MySQL: " . mysqli_connect_error();
}

//Gets the variable from the request
$id = $_REQUEST["id"]; //Order id received in function removerorder parameters


//Restores the items in the canceled order to the wares list again
$sql = $conn->prepare("SELECT Quantity, ObjectID FROM Ordered_Items WHERE OrderID = ?");
$sql->bind_param("i", $id);
$sql->execute();
$result = $sql->get_result();

while($row = mysqli_fetch_array($result)){
    try {
    $conn->begin_transaction();
    $sql = $conn->prepare("UPDATE Wares SET Quantity = Quantity + $row[Quantity] WHERE ObjectID = $row[ObjectID]");
    $sql->bind_param("i", $id);
    $sql->execute();
    $conn->commit();
    }
    catch (Exception $e) {
	$conn->rollback();
	echo "Error updating wares record: " . mysqli_error($conn);
    }
}

// Deletes order based on inputed OrderID ($id)
try {
	$conn->begin_transaction();
	$sql = $conn->prepare("Delete from Orders where OrderID = ?");
	$sql->bind_param("i", $id);
	$sql->execute();
	$conn->commit();
} catch (Exception $e) {
	$conn->rollback();
	echo "Error deleting record: " . mysqli_error($conn);
}



mysqli_close($conn);
}
?>