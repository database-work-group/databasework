<?php
function is_item_available()
{
    $conn = new mysqli("localhost", "root", "CHANGEPASSWORD", "cheeseshopDB");
    if($stmt2 = $conn->prepare("SELECT Wares.Quantity AS WareQuant, Cart.Quantity AS CartQuant FROM Wares INNER JOIN Cart ON Cart.ObjectID = Wares.ObjectID WHERE CartID=?")) {
        $stmt2->bind_param("i", $_COOKIE["user_cookie"]);
        $stmt2->execute();
        if($res = $stmt2->get_result()) {
            while($row = $res->fetch_assoc()) {
                $WareItemQuant = $row["WareQuant"];
                $CartItemQuant = $row["CartQuant"];
                if($CartItemQuant > $WareItemQuant) {
                    return false;
                }
            }
            return true;
        } else {
            die("No results");
        }
    } else {
        die("Prepared statement issue");
    }
}

?>