<?php
function cookie_login_check()
{
    $cookie_user = 'user_cookie';
    $cookie_password = 'password_cookie';
    if(!isset($_COOKIE[$cookie_password]) || !isset($_COOKIE[$cookie_user])) {
        echo "Cookie with names $cookie_user $cookie_password does not exist...";
        return false;
    } else {    
        $conn = new mysqli("localhost", "root", "CHANGEPASSWORD", "cheeseshopDB");
	    //Tries to cookie login
	    if($stmt = $conn->prepare("SELECT Password FROM Users WHERE UserID = ?")) {
		    $stmt->bind_param("i", $_COOKIE[$cookie_user]);
		    $stmt->execute();
		    $result = $stmt->get_result();
		    if($result->num_rows === 0) { //Checks if anything returned
			    echo "Cookie login failed";
                return false;
		    } else {
			    $row = $result->fetch_assoc();
			    if($_COOKIE[$cookie_password] != $row["Password"]){
				    echo "That is not the password for your account.";
                    return false;
			    } else {
                    return true;
                }
            }
        } else {
            echo "Issue with prepare statement";
            return false;
        }
    }
}

?>
