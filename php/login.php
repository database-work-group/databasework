<?php
$conn = new mysqli("localhost", "root", "CHANGEPASSWORD", "cheeseshopDB");
if($conn->connect_error) {
        die("Could not connect".$conn->connect_error);
}

if($stmt = $conn->prepare("SELECT FName, UserID, LName, Password, Privilege FROM Users WHERE Email = ?")) {
        $stmt->bind_param("s", $_POST["Email"]);
        $stmt->execute();
        $result = $stmt->get_result();
        if($result->num_rows === 0) { //Checks if anything returned
                header('Location: http://130.240.200.65/fail.html');
        } else {
    		$row = $result->fetch_assoc();
    		if(password_verify($_POST["Password"], $row["Password"])) {
    			// Here the keeplogin cookie is set for one day.
    			$cookie_user = 'user_cookie';
    			$cookie_password = 'password_cookie';
    			$cookie_name = 'name_cookie';
    		    $cookie_email = 'email_cookie';
    			setcookie($cookie_user, $row["UserID"], time() + (86400 * 30), "/"); // 86400 = 1 day
    			setcookie($cookie_password, $row["Password"], time() + (86400 * 30), "/"); // 86400 = 1 day
    			setcookie($cookie_name, $row["FName"], time() + (86400 * 30), "/"); // 86400 = 1 day
    			setcookie($cookie_email, $_POST["Email"], time() + (86400 * 30), "/"); // 86400 = 1 day
    			
    			$cookie_privilege = 'privilege_cookie';
    			if($row["Privilege"] == 2){
    			   	setcookie($cookie_privilege, 'Admin', time() + (86400 * 30), "/"); // 86400 = 1 day 
    			} elseif($row["Privilege"] == 1){
    			    setcookie($cookie_privilege, 'Seller', time() + (86400 * 30), "/"); // 86400 = 1 day 
    			} else{
    			    setcookie($cookie_privilege, 'Buyer', time() + (86400 * 30), "/"); // 86400 = 1 day 
    			}
    			
    			//header('Location: http://130.240.200.65/success.html');
    	        header('Location: http://130.240.200.65/index.php');
    		} else {
    			header('Location: http://130.240.200.65/fail.html');
    		}
        }
} else {
	echo "Could not prepare statement";
}

?>
