<?php
if(isset($_COOKIE["user_cookie"]) and isset($_COOKIE["password_cookie"]) and isset($_COOKIE["privilege_cookie"]) and isset($_COOKIE["name_cookie"]) and isset($_COOKIE["email_cookie"])) {
    setcookie('user_cookie', '', time()-3600);
    setcookie('password_cookie', '', time()-3600);
    setcookie('privilege_cookie', '', time()-3600);
    setcookie('name_cookie', '', time()-3600);
    setcookie('email_cookie', '', time()-3600);
    echo "You have now logged out";
    header("Location: http://130.240.200.65/index.php");
}
?>