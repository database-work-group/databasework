<?php
include "cookie_login_check.php";
if(cookie_login_check()){

$cookie_user = 'user_cookie';
$cookie_password = 'password_cookie';


$conn = new mysqli("localhost", "root", "CHANGEPASSWORD", "cheeseshopDB");
// Check connection
if (mysqli_connect_errno())
{
//echo "Failed to connect to MySQL: " . mysqli_connect_error();
}


$ID = $_REQUEST["id"];
$Q = $_REQUEST["q"];
        
        $result = mysqli_query($conn,"SELECT Quantity FROM Cart Where ObjectID = $ID AND CartID = $_COOKIE[$cookie_user]");
        $row = mysqli_fetch_array($result);
        
        if($Q > $row["Quantity"]){
            echo "You can't remove that many items of this ware. Lower your quantity specification please. <br>";
        }
        elseif($Q == $row["Quantity"]){
            //Remove the item from the cart
			try {
				$conn->begin_transaction();
				$sql = $conn->prepare("DELETE FROM Cart WHERE ObjectID = ? AND CartID = ?");
				$sql->bind_param("ii", $ID, $_COOKIE[$cookie_user]);
				$sql->execute();
				$conn->commit();
				echo "Cart was updated, deleted";
			} catch (Exception $e) {
				$conn->rollback();
				echo "Error deleting record: " . mysqli_error($conn);
			}
        }
        else{
            //Decrease the quantity
			try {
				$conn->begin_transaction();
				$sql = $conn->prepare("UPDATE Cart SET Quantity = Quantity - ? WHERE ObjectID = ? AND CartID = ?");
				$sql->bind_param("iii", $Q, $ID, $_COOKIE[$cookie_user]);
				$sql->execute();
				$conn->commit();
				echo "Cart was updated, decreased quantity";
			} catch (Exception $e) {
				$conn->rollback();
				echo "Error updating record: " . mysqli_error($conn);
			}

        }
mysqli_close($conn);
}
?>