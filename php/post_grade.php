<?php
include 'cookie_login_check.php';

$servername="localhost";
$username="root";
$password="CHANGEPASSWORD";
$dbname="cheeseshopDB";

$conn = new mysqli($servername, $username, $password, $dbname);
if($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
} else {
	if(!cookie_login_check()) {
		die("Issue with login details!");
	}
	//Check if user has already posted a review
	if($stmtCheck = $conn->prepare("SELECT ReviewID FROM Reviews WHERE UserID = ? AND ReviewID = ?")) {
		$stmtCheck->bind_param("ii", $_COOKIE["user_cookie"], $_POST["ObjectID"]);
		$stmtCheck->execute();
		$res = $stmtCheck->get_result();
		if($res->num_rows != 0) {
			echo "You have already reviewed this product!<br>";
			echo "<a href=".$_SERVER['HTTP_REFERER'].">Click to go back to the product page</a>";
		} else {
			if($stmt = $conn->prepare("SELECT Grade, NumGrade FROM Wares WHERE ObjectID = ?")) {
				$stmt->bind_param("i", $_POST["ObjectID"]);
				$stmt->execute();
				$res = $stmt->get_result();
				$row = $res->fetch_assoc();
				$curGrade = $row["Grade"];
				try {
				    $conn->begin_transaction();
    				if($stmt2 = $conn->prepare('UPDATE Wares SET Grade = ?, NumGrade = ? WHERE ObjectID = ?')) {
    					$userGrade = $_POST["Grade"];
    					$n = $row["NumGrade"];
    					$newGrade = ($n*$curGrade)/($n+1) + $userGrade/($n+1);
    					echo "NumGrade:", $n, " Current Grade: ", $curGrade, " User Grade: ", $userGrade, " New Grade: ", $newGrade;
    					$newNumGrade = $n+1;
    					$stmt2->bind_param("dii", $newGrade, $newNumGrade, $_POST["ObjectID"]);
    					$stmt2->execute();
    				} else {
    					die('prepare() failed: ' . htmlspecialchars($conn->error));
    				}
    				if($stmt3 = $conn->prepare("INSERT INTO Reviews(ReviewID, UserID, Review, Grade) VALUES (?, ?, ?, ?)")) {
    					$stmt3->bind_param("iisi", $_POST["ObjectID"], $_COOKIE["user_cookie"], $_POST["Review"], $_POST["Grade"]);
    					echo "This is the review: ", $_POST["Review"];
    					$stmt3->execute();
    					$conn->commit();
    					header('Location: '.$_SERVER['HTTP_REFERER']);
    				} else {
    					die('prepare() failed: '. htmlspecialchars($conn->error));
    				}
				} catch (Exception $e) {
				    $conn->rollback();
				}
			} else {
				echo "Error with prepared statement!";
			}
		}
	} else {
		echo "Error with prepared statement!";
	}


}
?>
