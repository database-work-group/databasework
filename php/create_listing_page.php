<?php
//echo "Creating New Page!!!";
$conn = new mysqli("localhost", "root", "CHANGEPASSWORD", "cheeseshopDB");
// Check connection
if (mysqli_connect_errno()) {
        echo "Failed to connect to MySQL: " . mysqli_connect_error();
} else {
        $result = mysqli_query($conn,"SELECT ObjectID, Item, Type, Grade, Price, Image, Description FROM Wares");
        while($row = mysqli_fetch_array($result)) {
                $phpName = $row["Item"];
                $file = $phpName.".html";
                if($f = fopen($_SERVER['DOCUMENT_ROOT'].'/test/'.$phpName, 'w')) {
                        $wbpgContent = '<html>
			 <head>
			<link rel="stylesheet" type="text/css" href="/flexbox.css">
			<link rel="stylesheet" type="text/css" href="review.css">
			</head>
			<header>
			    Item:'. $row["Item"].'
			</header>
			<script>
			function getGrade() {
				var xmlhttp = new XMLHttpRequest();
				xmlhttp.onreadystatechange = function() {
					if (this.readyState == 4 && this.status == 200) {
						document.getElementById("grade").innerHTML = this.responseText;
					}
				};
				xmlhttp.open("GET", "getgrade.php?q=" + '.$row["ObjectID"].', true);
				xmlhttp.send();
			}
			function getReviews() {
				var xmlhttp = new XMLHttpRequest();
				xmlhttp.onreadystatechange = function() {
					if (this.readyState == 4 && this.status == 200) {
						document.getElementById("reviews").innerHTML = this.responseText;
					}
				};
				xmlhttp.open("GET", "getreviews.php?q=" + '.$row["ObjectID"].', true);
				xmlhttp.send();
			}
			window.onload = function() { 
			    getGrade();
			    getReviews();
			}
			</script>
			<section>
			    <nav>
			        <a href="/index.php">Home page</a><br>
				<a href="/pagetest.html">Listings</a><br>
			    </nav>
			    <article>
			        Description:'.$row["Description"].'<br>
			        Type:'. $row["Type"].'<br>
			        Price:'. $row["Price"].'<br>
				Grade: <text id = "grade"></text> <br>
				<img src="/images/'.$row["Image"].'" alt="Picture of '.$row["Item"].'">
				<form action="post_grade.php" method="post">
			        <input type="hidden" name="ObjectID" value="'.$row["ObjectID"].'">
			        <input type="radio" name="Grade" value="1"> 1 <br>
			        <input type="radio" name="Grade" value="2"> 2 <br>
			        <input type="radio" name="Grade" value="3"> 3 <br>
			        <input type="radio" name="Grade" value="4"> 4 <br>
			        <input type="radio" name="Grade" value="5"> 5 <br>
			        <textarea name="Review" id="Review" rows="10" cols="30"></textarea>
			        <input type="submit" value="Submit">
			        </form>
				<div id = "reviews"></div>
			    </arcticle>
			</section>
			<footer>
                <p>&copy; Copyright 2018, Cheese Co.</p>
            </footer>


			</html>';
                        fwrite($f, $wbpgContent);
                        fclose($f);
                } else {
                        echo "Failed to create file";
                }
        }
}
?>


