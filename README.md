# Database ecommerce project

Coding style:  
Follow the style shown here https://www.kernel.org/doc/html/v4.10/process/coding-style.html  

Use this to check for errors in the php code running on the server:  
sudo tail /var/log/apache2/error.log

Update Web Server contents:  
cd dbproj/databasework  
git pull origin master  
sudo ./move.sh  