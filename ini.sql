DROP DATABASE cheeseshopDB;
CREATE DATABASE cheeseshopDB;
USE cheeseshopDB;

CREATE TABLE Users(UserID int NOT NULL AUTO_INCREMENT PRIMARY KEY,
FName varchar(255) NOT NULL,
LName varchar(255),
Email varchar(255) NOT NULL,
Address varchar(1024),
Privilege int NOT NULL,
Password varchar(255) NOT NULL);

CREATE TABLE Wares(ObjectID int NOT NULL AUTO_INCREMENT PRIMARY KEY,
SellerID int NOT NULL,
FOREIGN KEY fk_SellerID(SellerID)
REFERENCES Users(UserID)
ON DELETE CASCADE
ON UPDATE CASCADE,
Item varchar(255) NOT NULL,
Type varchar(255) NOT NULL,
Description varchar(1024) NOT NULL,
Image varchar(255),
Grade decimal(3, 2) NOT NULL,
NumGrade int NOT NULL,
Price int NOT NULL,
Quantity int NOT NULL,
Discount int);

CREATE TABLE Orders(OrderID int NOT NULL AUTO_INCREMENT PRIMARY KEY,
UserID int NOT NULL,
FOREIGN KEY fk_UserID(UserID)
REFERENCES Users(UserID)
ON DELETE CASCADE
ON UPDATE CASCADE,
Address varchar(1024) NOT NULL,
OrderDate int NOT NULL,
ArriveDate int NOT NULL,
Priority int NOT NULL,
TotalPrice int NOT NULL,
Status int NOT NULL);

CREATE TABLE Ordered_Items(
OrderID int NOT NULL,
FOREIGN KEY fk_OrderID(OrderID)
REFERENCES Orders(OrderID)
ON DELETE CASCADE
ON UPDATE CASCADE,
ObjectID int NOT NULL,
FOREIGN KEY fk_ObjectID(ObjectID)
REFERENCES Wares(ObjectID)
ON DELETE CASCADE
ON UPDATE CASCADE,
Quantity int NOT NULL,
Price int NOT NULL);

CREATE TABLE Cart(CartID int NOT NULL,
FOREIGN KEY fk_CartID(CartID)
REFERENCES Users(UserID)
ON DELETE CASCADE
ON UPDATE CASCADE,
ObjectID int NOT NULL,
FOREIGN KEY fk_ObjectID(ObjectID)
REFERENCES Wares(ObjectID)
ON DELETE CASCADE
ON UPDATE CASCADE,
Quantity int NOT NULL);

CREATE TABLE Reviews(ReviewID int NOT NULL,
FOREIGN KEY fk_ReviewID(ReviewID)
REFERENCES Wares(ObjectID)
ON DELETE CASCADE
ON UPDATE CASCADE,
UserID int NOT NULL,
FOREIGN KEY fk_UserID(UserID)
REFERENCES Users(UserID)
ON DELETE CASCADE
ON UPDATE CASCADE,
Review text,
Grade int NOT NULL);

CREATE TABLE AdminCode(aCode varchar(255) not NULL);
Insert INTO AdminCode(aCode) VALUES (1234);


CREATE INDEX idx_users_email
ON Users (Email);
CREATE INDEX idx_wares_fk_searches
ON Wares (SellerID, Item, Type);
CREATE INDEX idx_orders_fk
ON Orders (UserID, Address);
CREATE INDEX idx_ordereditems_fk_searches
ON Ordered_Items (OrderID, ObjectID);
CREATE INDEX idx_cart_fk
ON Cart (CartID, ObjectID);
CREATE INDEX idx_reviews_fk
ON Reviews (ReviewID, UserID);

