<html>
	<body>
		<head>
			<link rel="stylesheet" href="flexbox.css" type="text/css">
		    <link rel="stylesheet" href="general.css" type="text/css">
			<title>Cheese Shop</title>
		</head>
		
		
		<script>
window.addEventListener("load", myInit, true); function myInit(){getWares("");};
var waretable = "";
var warehead = "<th>My Wares</th><tr><th>Item</th><th>Type</th><th>Quantity</th><th>Price</th><th></th></tr>";
 
function getWares(search) {
    
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            waretable = warehead + this.responseText;
            document.getElementById("warestable").innerHTML = waretable;
        }
    };
    xmlhttp.open("GET", "get_my_wares.php?q=" + search, true);
    xmlhttp.send();

}

function removeItem(id){
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById("txtHint").innerHTML = "removeItem ran id = " + id;
            getWares("");
        }
    };
    xmlhttp.open("GET", "remove_wares.php?id=" + id, true);
    xmlhttp.send();
}

</script>

		
		
		<header>
			<b>Another <img src="/images/cheese_logo.png" alt="C" style="width:32px;height:32px;">heese shop</b>
			<section>
				<font size="1"><?php 
				$cookie_name = 'name_cookie';
				$cookie_email = 'email_cookie';
				$cookie_privilege = 'privilege_cookie';
				if (!isset($_COOKIE[$cookie_email]) || !isset($_COOKIE[$cookie_name]) || !isset($_COOKIE[$cookie_privilege])) {
					echo "You are not logged in yet <br>";
				} else {
					echo "$_COOKIE[$cookie_name] <br>";
					echo "$_COOKIE[$cookie_email] <br>";
					echo "$_COOKIE[$cookie_privilege] <br>";
				}
				?></font>
			</section>
		</header>

		<section>
			<?php
			include 'links.php';
			?>

			<article>
			<p><b>Start typing a searchword into the field below</b></p>
            <form> 
            Search: <input type="text" onkeyup="getWares(this.value)">
            </form>
            <p>Outprint: <span id="txtHint"></span></p>
            <div float:left>
                <table id="warestable" align="left" width= "50%"; background-color = "blue";>
                </table>
            </div>
            </article>
            
		</section>
		<footer>
			<p>&copy; Copyright 2018, Cheese Co.</p>
		</footer>
</html>
