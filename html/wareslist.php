<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="general.css" type="text/css">
    <link rel="stylesheet" href="flexbox.css" type="text/css">
<title>Cheese Shop</title>
</head>
<body>

<script>
window.addEventListener("load", myInit, true); function myInit(){getWares(""); getCart();};
var waretable = "";
var warehead = "<th>Wares</th><tr><th>Item</th><th>Type</th><th>Quantity</th><th>Price</th><th></th></tr>";
 
var carttable = ""
var carthead = "<th>My Cart</th><tr><th>Item</th><th>Type</th><th>Quantity</th><th>Price</th><th></th></tr>";

function getWares(str) {
    
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            waretable = warehead + this.responseText;
            document.getElementById("warestable").innerHTML = waretable;
        }
    };
    xmlhttp.open("GET", "getwares.php?q=" + str, true);
    xmlhttp.send();

}

function getCart(){
    carttable = carthead;
    document.getElementById("cartwares").innerHTML = carttable;
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            carttable = carthead + this.responseText;
            document.getElementById("cartwares").innerHTML = carttable;
        }
    };
    xmlhttp.open("GET", "getcart.php", true);
    xmlhttp.send();
}

function addToCart(id){
    var quantity = document.getElementById("ware"+id).value;
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById("txtHint").innerHTML = "addToCart ran id = " + id + " and quantity = " + quantity;
            getCart();
        }
    };
    xmlhttp.open("GET", "addtocart.php?id=" + id + "&q=" + quantity, true);
    xmlhttp.send();
}

function removeFromCart(id){
    var quantity = document.getElementById("cart"+id).value;
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById("txtHint").innerHTML = "removeFromCart ran id = " + id + " and quantity = " + quantity;
            getCart();
        }
    };
    xmlhttp.open("GET", "removefromcart.php?id=" + id + "&q=" + quantity, true);
    xmlhttp.send();
}

</script>

<header>
  <b>Another <img src="/images/cheese_logo.png" alt="C" style="width:32px;height:32px;">heese shop</b>
  <section>
  <font size="1"><?php 
  $cookie_name = 'name_cookie';
  $cookie_email = 'email_cookie';
  $cookie_privilege = 'privilege_cookie';
  if (!isset($_COOKIE[$cookie_email]) || !isset($_COOKIE[$cookie_name]) || !isset($_COOKIE[$cookie_privilege])) {
      echo "You are not logged in yet <br>";
  } else {
      echo "$_COOKIE[$cookie_name] <br>";
      echo "$_COOKIE[$cookie_email] <br>";
      echo "$_COOKIE[$cookie_privilege] <br>";
  }
  ?></font>
  </section>
</header>

<section>
<?php
 	 include 'links.php';
	 ?>


  <article>
<p><b>Start typing a searchword into the field below</b></p>
<form> 
Search: <input type="text" onkeyup="getWares(this.value)">
</form>
<p>Outprint: <span id="txtHint"></span></p>
<div float:left>
    <table id="warestable" align="left" width= "50%"; background-color = "blue";>
    </table>
    <table id="cartwares" align="left" width= "50%">
    </table>
</div>
  </article>
</section>

<footer>
  <p>&copy; Copyright 2018, Cheese Co.</p>
</footer>





</body>
</html>