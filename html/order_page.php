<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="general.css" type="text/css">
    <link rel="stylesheet" href="flexbox.css" type="text/css">
    <title>Cheese Shop</title>
</head>


<body>



<script>
window.addEventListener("load", myInit, true); function myInit(){getorder("");};
var ordertable = "";
var orderhead = "<th>Orders</th> <tr> <th>OrderID</th> <th>OrderDate</th> <th>ArriveDate</th> <th>Address</th> <th>Priority</th> <th>TotalPrice</th> <th>Status</th> <th></th> </tr>";
function getorder(search){
    ordertable = orderhead;
    document.getElementById("Ordertable").innerHTML = ordertable;
    
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            ordertable = ordertable + this.responseText;
            document.getElementById("Ordertable").innerHTML = ordertable;
        }
    };
    xmlhttp.open("GET", "order_history.php?q=" + search, true);
    xmlhttp.send();
}


var ordereditemstable = "";
var ordereditemshead = "<th>More</th><tr><th>SellerFname</th> <th>SellerLname</th> <th>Item</th> <th>Type</th> <th>Grade</th> <th>Price</th> <th>Quantity</th> <th>Discount</th> </tr>";

// this function should pass through the order id and then get all the items of that order
function showitems(id){
    ordereditemstable = ordereditemshead;
    document.getElementById("Orderitem").innerHTML = ordereditemstable;
    
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
    
            ordereditemstable = ordereditemstable + this.responseText;
            document.getElementById("Orderitem").innerHTML = ordereditemstable;
        }
    };
    xmlhttp.open("GET", "ordered_items_history.php?id="+ id, true);
    xmlhttp.send();
}


function removeorder(id){
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById("test").innerHTML = "removeorder ran id = " + id + this.responseText;
            getorder("");
            }
    };
xmlhttp.open("GET", "remove_order.php?id=" + id, true);
xmlhttp.send();
}


function nextstatus(id){
    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            document.getElementById("test").innerHTML = "nextstatus ran id = " + id + this.responseText;
            getorder("");
            }
    };
xmlhttp.open("GET", "next_status.php?id=" + id, true);
xmlhttp.send();
}


</script>

 
 <header>
  <b>Another <img src="/images/cheese_logo.png" alt="C" style="width:32px;height:32px;">heese shop</b>
  <section>
  <font size="1"><?php 
  $cookie_name = 'name_cookie';
  $cookie_email = 'email_cookie';
  $cookie_privilege = 'privilege_cookie';
  if (!isset($_COOKIE[$cookie_email]) || !isset($_COOKIE[$cookie_name]) || !isset($_COOKIE[$cookie_privilege])) {
      echo "You are not logged in yet <br>";
  } else {
      echo "$_COOKIE[$cookie_name] <br>";
      echo "$_COOKIE[$cookie_email] <br>";
      echo "$_COOKIE[$cookie_privilege] <br>";
  }
  ?></font>
  </section>
</header>

<section>
    <?php
	include 'links.php';
	?>

<article>
<p id = "test"></p> 
<form> 
Search: <input type="text" onkeyup="getorder(this.value)">
</form>

    <!--<p><button type="button" id="demo" onclick="showitems(1)">Click me to change my HTML content (innerHTML).</p>-->
<div>
    <table id="Ordertable" align="left" width="50%">
    </table>
    <table id="Orderitem" align="left" width="50%">
    </table>
</div>
</article>
</section>

<footer>
  <p>&copy; Copyright 2018, Cheese Co.</p>
</footer>


    
</body>
</html>