<html>
	<body>
		<head>
			<link rel="stylesheet" href="general.css" type="text/css">
			<link rel="stylesheet" href="flexbox.css" type="text/css">
			<title>Cheese Shop</title>
		</head>
		<header>
			<b>Another <img src="/images/cheese_logo.png" alt="C" style="width:32px;height:32px;">heese shop</b>
			<section>
				<font size="1"><?php 
				$cookie_name = 'name_cookie';
				$cookie_email = 'email_cookie';
				$cookie_privilege = 'privilege_cookie';
				if (!isset($_COOKIE[$cookie_email]) || !isset($_COOKIE[$cookie_name]) || !isset($_COOKIE[$cookie_privilege])) {
					echo "You are not logged in yet <br>";
				} else {
					echo "$_COOKIE[$cookie_name] <br>";
					echo "$_COOKIE[$cookie_email] <br>";
					echo "$_COOKIE[$cookie_privilege] <br>";
				}
				?></font>
			</section>
		</header>

		<section>
			<?php
			include 'links.php';
			?>

			<article>
				<p>
        		<form action="post_ware.php" method="post" enctype="multipart/form-data">
				Item name: <input type="text" name="Item"><br>	
				Item type: <input type="text" name="Type"><br>
				Description: <input type="text" name="Description"><br>
				ImageUrl: <input type="file" name="Image" id="Image" accept="image/*"><br>
				Price: <input type="text" name="Price"><br>
				Quantity: <input type="text" name="Quantity"><br>
				Discount: <input type="text" name="Discount"><br>
				<input type="submit">
				</form>
				</p>
			</article>
		</section>
		<footer>
			<p>&copy; Copyright 2018, Cheese Co.</p>
		</footer>
</html>