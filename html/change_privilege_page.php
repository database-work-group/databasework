<html>
	<body>
		<head>
			<link rel="stylesheet" href="general.css" type="text/css">
			<link rel="stylesheet" href="flexbox.css" type="text/css">
			<title>Cheese Shop</title>
		</head>
		<header>
			<b>Another <img src="/images/cheese_logo.png" alt="C" style="width:32px;height:32px;">heese shop</b>
			<section>
				<font size="1"><?php 
				$cookie_name = 'name_cookie';
				$cookie_email = 'email_cookie';
				$cookie_privilege = 'privilege_cookie';
				if (!isset($_COOKIE[$cookie_email]) || !isset($_COOKIE[$cookie_name]) || !isset($_COOKIE[$cookie_privilege])) {
					echo "You are not logged in yet <br>";
				} else {
					echo "$_COOKIE[$cookie_name] <br>";
					echo "$_COOKIE[$cookie_email] <br>";
					echo "$_COOKIE[$cookie_privilege] <br>";
				}
				?></font>
			</section>
		</header>

		<section>
			<?php
			include 'links.php';
			?>

			<article>
				<p>
                <form action="change_privilege.php" method="post">
                User email: <input type="text" name="Email"><br>  
                <input type="submit">  
				</p>
			</article>
		</section>
		<footer>
			<p>&copy; Copyright 2018, Cheese Co.</p>
		</footer>
</html>
