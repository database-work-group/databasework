#!/bin/bash

cd ..
read -sp 'Type in database password: ' dbpass
find databasework/* -type f ! -name '*.sh' -exec sed -i 's/CHANGEPASSWORD/'$dbpass'/g' {} \;
cd databasework

rsync -a -u php /var
rsync -a -u html /var/www
rsync -a -u test /var/www/html
chown -R www-data /var/www/html/images
chown -R www-data /var/www/html/test


service apache2 restart
